<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
</head>
<body>
  <?php
  include('nav.php');
  ?>
    <div class="bg-atas">
        <div class="title">
            <p class="h1 text-white m-lg-auto"><b>SELAMAT DATANG DI WEB WISATA KOTA BENGKULU</b></p>
        </div>
    </div>
    <div class="bawah">
        <p class="h5 text-white" style="text-align: center;">Scrol <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="white" class="bi bi-arrow-down-squarek"  viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M15 2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2zM0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm8.5 2.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"/>
        </svg></p>
    </div>
    <!--content-->
    <div class="row row-cols-0 row-cols-md-2 g-1">
    <div class="col">
      <div class="card" style="text-align: center; overflow: hidden; padding: 0;">
        <img style="max-height: 375px;" src="img/pantai panjang 4.jpg" class="card-img-top" alt="...">
        <div class="card-block">
          <h5 class="card-title text-center">Pantai Panjang</h5>
          <center><a href="#" class="btn btn-primary">Lihat</a></center>
        </div>
      </div>
    </div>

    <div class="col">
      <div class="card" style="text-align: center; overflow: hidden; padding: 0;">
        <img style="max-height: 375px;" src="img/danau.jpg" class="card-img-top" alt="...">
        <div class="card-block">
          <h5 class="card-title text-center">Danau Dendam Tak Sudah</h5>
          <center><a href="#" class="btn btn-primary">Lihat</a></center>
          
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card" style="text-align: center; overflow: hidden; padding: 0;">
        <img style="max-height: 375px;" src="img/Benteng-Marlborough.jpg" class="card-img-top" alt="...">
        <div class="card-block">
          <h5 class="card-title text-center">Benteng-Marlborough</h5>
          <center><a href="#" class="btn btn-primary">Lihat</a></center>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card" style="text-align: center; overflow: hidden; padding: 0;">
        <img style="max-height: 375px;" src="img/rumah.jpg" class="card-img-top" alt="...">
        <div class="card-block">
          <h5 class="card-title text-center">Rumah Bung Karno</h5>
          <center><a href="#" class="btn btn-primary">Lihat</a></center>
        </div>
        </div>
      </div>
    </div>
</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
       <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
<?php
include('footer.html');
?>
</html>