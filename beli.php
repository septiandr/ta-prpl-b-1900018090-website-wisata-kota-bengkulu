<?php
    require 'config.php';
    require 'rupiah.php';

    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <style type="text/css">
    .gradient2 {
	width: 100wh;
	height: 90vh;
	color: #fff;
	background: linear-gradient(-45deg, #7CFC00, #00FF00, #EE7752, #E73C7E, #23A6D5, #23D5AB, #FFFF00);
	background-size: 400% 400%;
	-webkit-animation: Gradient 15s ease infinite;
	-moz-animation: Gradient 15s ease infinite;
	animation: Gradient 15s ease infinite;
    }
    
    @-webkit-keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }
    
    @-moz-keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }
    
    @keyframes Gradient {
        0% {
            background-position: 0% 50%
        }
        50% {
            background-position: 100% 50%
        }
        100% {
            background-position: 0% 50%
        }
    }

    .foto{
        padding: 0; margin: 0;
        width: 1200px;
        margin: auto;
        background-image: linear-gradient(to right, #4682B4, #00FFFF, #00FA9A); 
        

	}
	.foto img {
        display: block;
        margin-left: auto;
        margin-right: auto;
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 9px;
        width: 1100px;
	}
    

</style>
</head>
<body>
    <?php
    include('nav.php');
    ?>

      <!-- jumbotron -->
    <section class="jumbotron-bg shadow  mb-3 rounded">
        <div class="jumbotron gradient2 text-white" style="height: 250px;">
        <div class="container">
        <br><br><br>
        <h1 class="display-2 fw-bolder"><center>OLEH OLEH</center> </h1>
        <p class="lead fw-normal"><center>Khas Kota BENGKULU</center></p>
        </div>
        </div>
    </section>
    <!-- penutup jumbotron -->

    <!--content-->
    <section>
        <div class="wrapper">
            <div class="gambar">

                <?php
                    $tampilProduk = $pdo->query("SELECT * FROM produk ORDER BY id_produk DESC");

                    while($rProduk = $tampilProduk->fetch(PDO::FETCH_ASSOC)){
                ?>
                <div class="card" style="width: 20rem;">
                    <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>">
                    <div class="card-body">
                        <h1><?= $rProduk['nama_produk']; ?></h1>
                        <p>Rp<?= rp($rProduk['harga']); ?></p><br>
                        <a href="transaksi.php?id_produk=<?= $rProduk['id_produk']; ?>">Beli Produk <i class="fas fa-cart-arrow-down"></i></a> 
                    </div>
                </div>

                <?php } ?>

            </div>
        </div>
    </section>

     <!-- Option 1: Bootstrap Bundle with Popper -->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
</html>    

        