<?php
    require 'config.php';
    require 'rupiah.php';

    session_start();

    if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
    <style type="text/css">
	body {
	  background-image: linear-gradient(to right, #4682B4, #00FFFF, #00FA9A);
	}
    </style>
</head>
<body>
<?php
include('nav.php');
?>
   
    <section>
        <div class="wrapper">
            <div class="form">
                <table class="table table-striped" style="color: #fff;text-align: center;">
                    <thead>
                        <tr>
                            <th colspan="4" style="color: #23dbdb;font-size: 24px;">DETAIL AKUN</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th scope="col">ID User</th>
                            <th scope="col">Nama User</th>
                            <th scope="col">Alamat Email</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tampilUser = $pdo->query("SELECT * FROM user WHERE id_user='$_SESSION[id_user]'");

                            $rUser = $tampilUser->fetch(PDO::FETCH_ASSOC);
                        ?>
                        <tr>
                            <td><?= $rUser['id_user']; ?></td>
                            <td><?= $rUser['nama_user']; ?></td>
                            <td><?= $rUser['email']; ?></td>
                            <td><?= $rUser['password']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="form">
                <table class="table table-striped" style="color: #fff;text-align: center;">
                    <thead>
                        <tr>
                            <th colspan="8" style="color: #23dbdb;font-size: 24px;">RIWAYAT TRANSAKSI SAYA</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th scope="col">ID Transaksi</th>
                            <th scope="col">ID Produk</th>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Harga Produk</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Total Bayar</th>
                            <th scope="col">Waktu</th>
                            <th scope="col">Bukti Transaksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tampilTransaksi = $pdo->query("SELECT 
                                                        produk.id_produk, produk.nama_produk, produk.harga, transaksi.qty, transaksi.id_transaksi, transaksi.total_bayar, transaksi.bukti_transaksi, transaksi.tanggal_transaksi
                                                        FROM transaksi
                                                        INNER JOIN produk ON transaksi.id_produk = produk.id_produk
                                                        WHERE transaksi.id_user='$_SESSION[id_user]'");

                            while ($rTransaksi = $tampilTransaksi->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                        <tr>
                            <td><?= $rTransaksi['id_transaksi']; ?></td>
                            <td><?= $rTransaksi['id_produk']; ?></td>
                            <td><?= $rTransaksi['nama_produk']; ?></td>
                            <td>Rp<?= rp($rTransaksi['harga']); ?></td>
                            <td><?= $rTransaksi['qty']; ?></td>
                            <td>Rp<?= rp($rTransaksi['total_bayar']); ?></td>
                            <td><?= $rTransaksi['tanggal_transaksi']; ?></td>
                            <td><img src="img/transaksi/<?= $rTransaksi['bukti_transaksi']; ?>" alt="Gambar BUkti Transaksi <?= $rTransaksi['bukti_transaksi']; ?>" style="width: 200px;"></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>