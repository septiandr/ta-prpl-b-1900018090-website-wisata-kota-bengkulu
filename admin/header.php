<?php
include("../config.php");

?>

<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <meta charset="utf-8">
    <title>Portal Admin | Bejualan</title>
    <style type="text/css">
	body {
	  background-image: linear-gradient(to right, #4682B4, #00FFFF, #00FA9A);
	}
    </style>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<title>ADMIN</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark text-white fixed-stop">
    	<div class="container">
    		<h1><i class="fa fa-user text-success mr-4" aria-hidden="true"></i></h1>
		  <a class="navbar-brand text-white">Web<b>BENGKULU</b><br>admin</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		    <li class="nav-item active">
		        <a class="nav-link" href="#"><span class="sr-only">(current)</span></a>
		    </li>
		    <li class="nav-item">
		        <a class="nav-link" href="#"</a>
		    </li>			
			<li class="nav-item">
			    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true"></a>
			</li>
			</ul>
			</div>
		</div>
	</nav>
	<div class="row">
		<div class="col-md-2 bg-light">
			<ul class="list-group list-group-flush p-2 pt-3">
  				<li class="list-group-item bg-success font-weight-bold text-dark" aria-disabled="true"><i class="fa fa-bars ml-1"></i> MENU</li>
  				<li class="list-group-item"><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="home.php">DASHBOARD</a></li>
 				<li class="list-group-item"><i class="fas fa-caret-downt" aria-hidden="true"></i><a href="#"> BARANG</a>
                    <ul>
                        <li><a href="daftarbarang.php">Daftar Barang</a></li>
                        <li><a href="tambahbarang.php">Tambah Barang</a></li>
                    </ul> 
            </li>
 				<li class="list-group-item"><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="rekappembelian.php"> Rekap Pembelian</a></li>
  				<li class="list-group-item"><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="dataadmin.php"> Data ADMIN</a></li>
                <li class="list-group-item"><i class="fa fa-chevron-right" aria-hidden="true"></i><a href="logout.php"> LOGOUT</a></li>
			</ul>
		</div>



		