<?php

    require '../config.php';

    if (isset($_POST['submit'])) {

        $username       = $_POST['username'];
        $password       = $_POST['password'];

        $queryLogin     = $pdo->query("SELECT * FROM admin WHERE username='$username' AND password='$password'");
        $rowsLogin      = $queryLogin->rowCount();
        $resultLogin    = $queryLogin->fetch(PDO::FETCH_ASSOC);

        if ($rowsLogin > 0){
            session_start();
            $_SESSION['id_admin']       = $resultLogin['id_admin'];
            $_SESSION['username']       = $resultLogin['username'];
            $_SESSION['password']       = $resultLogin['password'];

            echo "<script>alert('Berhasil login!'); window.location = 'home.php'</script>";
            exit();
        }else{
            echo "<script>alert('GAGAL! Mohon masukkan username & password dengan benar!!!'); window.location = 'home.php'</script>";
            exit();
        }

    }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Portal Admin | Bejualan</title>
    <link rel="stylesheet" href="style2.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
    <style type="text/css">
	body {
	  background-image: linear-gradient(to right, #4682B4, #00FFFF, #00FA9A);
	}
    </style>
</head>
</head>

<body> 
    <nav>
        <label class="logo">Web<b>BENGKULU</b> <small style="font-size: 12px;">PortalAdmin</small></label> 
        <ul>
            <li><a href="../"><i class="fas fa-home"></i>Home</a></li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">
            <div class="container">
                <h3 style="text-align: center;">PORTAL LOGIN ADMIN</h3>
                <br />
                <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
                    <div class="mb-3">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" min="0" required>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </section>
      <!-- Option 1: Bootstrap Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
</html>