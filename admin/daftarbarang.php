<?php
    require '../config.php';
    require '../rupiah.php';
    include ("header.php"); 

    session_start();

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
?>

    <section class="col-md-10">
        <div class="wrapper">
            <div class="form">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Produk</th>
                            <th scope="col">Harga</th>
                            <th scope="col" style="text-align: center;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            $tampil = $pdo->query("SELECT * FROM produk ORDER BY id_produk DESC");

                            while($r = $tampil->fetch(PDO::FETCH_ASSOC)){
                        ?>
                        <tr>
                            <th scope="row"><?= $no++; ?></th>
                            <td><?= $r['nama_produk']; ?></td>
                            <td>Rp<?= rp($r['harga']); ?></td>
                            <td style="text-align: center;">
                                <a href="editbarang.php?id=<?= $r['id_produk']; ?>" role="button" class="btn btn-primary">Edit <i class="far fa-edit"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>