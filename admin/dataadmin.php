<?php
    require '../config.php';
    include ("header.php");

    session_start();

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
?>
    <section class=col-md-10>
        <div class="wrapper">
            <div class="form">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Nama Admin</th>
                            <th scope="col">TTL</th>
                            <th scope="col">Umur</th>
                            <th scope="col">Username</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $tampilAdmin = $pdo->query("SELECT * FROM admin ORDER BY id_admin DESC");

                            while($rAdmin = $tampilAdmin->fetch(PDO::FETCH_ASSOC)){
                        ?>
                        <tr>
                            <td><?= $rAdmin['nama_admin']; ?></td>
                            <td><?= $rAdmin['ttl']; ?></td>
                            <td><?= $rAdmin['umur']; ?></td>
                            <td><?= $rAdmin['username']; ?></td>
                            <td><?= $rAdmin['password']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>