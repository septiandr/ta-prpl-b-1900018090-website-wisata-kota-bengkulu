<?php
    require '../conn.php';
    require '../rupiah.php';

    session_start();

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Portal Admin | Bejualan</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">Web<b>BENGKULU</b><small style="font-size: 12px;">PortalAdmin</small></label> 
        <ul>
            <li><a href="home.php">Dashboard</a></li>
            <li><a href="#">Data Produk <i class="fas fa-caret-down"></i></a>
                <ul>
                    <li><a href="daftar-produk.php">Daftar Produk</a></li>
                    <li><a href="tambah-produk.php">Tambah Produk</a></li>
                </ul> 
            </li>
            <li><a href="rekap-transaksi.php">Rekap Transaksi</a></li>
            <li><a class="active" href="data-user.php">Data User</a></li>
            <li><a href="data-admin.php">Data Admin</a></li>
            <li><a href="logout.php">Logout</a></li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">
            <div class="form">
                <table class="table table-striped" style="color: #fff;text-align: center;">
                    <thead>
                        <tr>
                            <th colspan="5" style="color: #23dbdb;font-size: 24px;">DATA USER</th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">ID User</th>
                            <th scope="col">Nama User</th>
                            <th scope="col">Alamat Email</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no=1;
                            $tampilUser = $pdo->query("SELECT * FROM user ORDER BY id_user DESC");

                            $rUser = $tampilUser->fetch(PDO::FETCH_ASSOC);
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $rUser['id_user']; ?></td>
                            <td><?= $rUser['nama_user']; ?></td>
                            <td><?= $rUser['email']; ?></td>
                            <td><?= $rUser['password']; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</body>
</html>