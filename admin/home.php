<?php

    require '../config.php';
    require '../rupiah.php';
    include ("header.php"); 
    session_start();

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }
    
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
     
    <section class="col-md-10">
        <div class="gambar">

            <?php

                // Jumlah Transaksi
                $tampilJmlTransaksi = $pdo->query("SELECT COUNT(id_transaksi) AS jumlahTransaksi FROM transaksi");
                $rJmlTransaksi      = $tampilJmlTransaksi->fetch(PDO::FETCH_ASSOC);

                // Brang Terjual
                $tampilBrgTerjual = $pdo->query("SELECT SUM(qty) AS barangTerjual FROM transaksi");
                $rBrgTerjual      = $tampilBrgTerjual->fetch(PDO::FETCH_ASSOC);

                // Total Penghasilan
                $tampilTotalPenghasilan = $pdo->query("SELECT SUM(total_bayar) AS totalPenghasilan FROM transaksi");
                $rTotalPenghasilan      = $tampilTotalPenghasilan->fetch(PDO::FETCH_ASSOC);

            ?>

            <div class="form">

                <div class="foto" style="padding-top: 10px;">
                    <!-- <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>"> -->
                    <h1 style="text-white">Jumlah Transaksi</h1>
                    <h3><?= rp($rJmlTransaksi['jumlahTransaksi']); ?></h3><br>
                    <a href="rekappembelian.php">Detail <i class="fas fa-angle-double-right"></i></a> 
                </div>
            </div>

            <div class="form">
                <div class="foto" style="padding-top: 10px;">
                    <!-- <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>"> -->
                    <h1 style="text-white;">Barang Terjual</h1>
                    <h3><?= rp($rBrgTerjual['barangTerjual']); ?></h3><br>
                    <a href="rekappembelian.php">Detail <i class="fas fa-angle-double-right"></i></a> 
                </div>
            </div>

            <div class="form">
                <div class="foto" style="padding-top: 10px;">
                    <!-- <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>"> -->
                    <h1 style="text-white;">Total Penghasilan</h1>
                    <h3>Rp<?= rp($rTotalPenghasilan['totalPenghasilan']); ?></h3><br>
                    <a href="rekappembelian.php">Detail <i class="fas fa-angle-double-right"></i></a> 
                </div>
            </div>

        </div>
    </section>
</body>
</html>