<?php

    require '../config.php';
    include ("header.php"); 
    session_start();

    if (empty($_SESSION['id_akun']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }

    $tampilEdit = $pdo->query("SELECT * FROM produk WHERE id_produk='$_GET[id]'");
    $rEdit      = $tampilEdit->fetch(PDO::FETCH_ASSOC);

    if (isset($_POST['submit'])) {

        $nama_produk    = $_POST['nama_produk'];
        $harga          = $_POST['harga'];

        if (empty($_FILES['gambar']['name'])) {
            $nama_gambar    = $rEdit['gambar'];
        }else{
            // Include Gambar
            $nama_gambar    = "produk-Khas-Bengkulu-".rand(00,99)."-".$_FILES['gambar']['name']; // mendapatkan nama gambar
            $lokasi_gambar  = $_FILES['gambar']['tmp_name']; // mendapatkan lokasi gambar
            $tujuan_gambar  = '../img'; // pindah gambar tersebut ke lokasi ini
            $upload_gambar  = move_uploaded_file($lokasi_gambar, $tujuan_gambar.'/'.$nama_gambar); // function mengupload/memindahkan file ke direktori yang di maksud
        }
        
        try {
            $stmt = $pdo->prepare("UPDATE produk SET nama_produk = :nama_produk, harga = :harga, gambar = :gambar WHERE id_produk='$_GET[id]' " );
                        
            $stmt->bindParam(":nama_produk", $nama_produk, PDO::PARAM_STR);
            $stmt->bindParam(":harga", $harga, PDO::PARAM_STR);
            $stmt->bindParam(":gambar", $nama_gambar, PDO::PARAM_STR);

            $count = $stmt->execute();

            $insertId = $pdo->lastInsertId();

            echo "<script>alert('Produk anda berhasil di edit!'); window.location = 'daftarbarang.php'</script>";
            
        }catch(PDOException $e){
            echo "<script>window.alert('Gagal edit produk'); window.location(history.back())</script>";
            exit();
        }

    }

?>

    <section class="col-md-10">
        <div class="wrapper">
            <div class="form">
                <h3 style="text-align: center;">EDIT PRODUK <u>"<?= $rEdit['nama_produk']; ?>"</u></h3>
                <br />
                <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
                    <div class="mb-3">
                        <label for="nama_produk" class="form-label">Nama Produk</label>
                        <input type="text" class="form-control" id="nama_produk" name="nama_produk" value="<?= $rEdit['nama_produk']; ?>" required>
                    </div>
                    <div class="mb-3">
                        <label for="harga" class="form-label">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga" value="<?= $rEdit['harga']; ?>" min="0" required>
                    </div>
                    <div style="margin-top: 10px;margin-bottom: 10px;">
                        <img src="../img/<?= $rEdit['gambar']; ?>" alt="<?= $rEdit['nama_produk']; ?>" style="width: 50%">
                        <br>
                    </div>
                    <div class="mb-3">
                        <label for="gambar" class="form-label">Gambar Produk</label>
                        <input class="form-control" type="file" id="gambar" name="gambar">
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                </form>
            </div>
        </div>
    </section>
</body>
</html>