<?php

    require 'config.php';

    if (isset($_POST['submit'])) {

        $email          = $_POST['email'];
        $password       = $_POST['password'];

        $queryLogin     = $pdo->query("SELECT * FROM user WHERE email='$email' AND password='$password'");
        $rowsLogin      = $queryLogin->rowCount();
        $resultLogin    = $queryLogin->fetch(PDO::FETCH_ASSOC);

        if ($rowsLogin > 0){
            session_start();
            $_SESSION['id_user']    = $resultLogin['id_user'];
            $_SESSION['email']      = $resultLogin['email'];
            $_SESSION['password']   = $resultLogin['password'];

            echo "<script>alert('Berhasil login!'); window.location = 'account.php'</script>";
            exit();
        }else{
            echo "<script>alert('GAGAL! Mohon masukkan email & password dengan benar!!!'); window.location = 'login.php'</script>";
            exit();
        }

    }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Login Akun | WebBENGKULU</title>
    <link rel="stylesheet" href="style-login.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">WebBENGKULU<small style="font-size: 12px;">PortalUser</small></label> 
        <ul>
            <li><a href="index.php"><i class="fas fa-home" style="text-decoration:none">HOME</i> </a></li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">
        <div class="container">
        <div class="row content">
            <div class="col-md-6 mb-3">
        </div>
            <div class="form">
                <h3 style="text-align: center;">PORTAL LOGIN AKUN</h3>
                <br />
                <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
                    <div class="mb-3">
                        <label for="email" class="form-label">Alamat Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Alamat Email" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" min="0" required>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary" name="submit">Login <i class="fas fa-sign-in-alt"></i></button>

                    <br />
                    <br />
                    <p>BELUM PUNYA AKUN?</p>
                    <br />

                    <a href="daftar.php" role="button" class="btn btn-outline-primary" style="text-decoration:none">DAFTAR AKUN <i class="fas fa-grip-lines-vertical"></i></a>
                    <a href="admin/index.php" role="button" class="btn btn-outline-primary" style="text-decoration:none">LOGIN ADMIN</a>
                </form>
            </div>
        </div>
    </section>
</body>
</html>

