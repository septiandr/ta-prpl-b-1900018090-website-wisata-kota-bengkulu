<?php

    require 'config.php';
    require 'rupiah.php';
    session_start();

    if (empty($_SESSION['id_user']) AND empty($_SESSION['email']) AND empty($_SESSION['password'])) {
        echo "<script>alert('Silahkan login dulu!'); window.location = 'logout.php'</script>";
    }

    if (empty($_GET['id_produk'])) {
        echo "<script>alert('Silahkan produk dulu!'); window.location = 'beli.php'</script>";
    }else{
        $tampilProduk = $pdo->query("SELECT * FROM produk WHERE id_produk='$_GET[id_produk]'");
        $rProduk = $tampilProduk->fetch(PDO::FETCH_ASSOC);
    }

    if (isset($_POST['submit'])) {

        // Di bawah ini untuk menampung inputan form yang diisi
        $id_user    = $_SESSION['id_user'];
        $id_produk  = $_POST['id_produk'];
        $qty        = $_POST['qty'];

        $total_bayar    = $qty*$rProduk['harga'];

        date_default_timezone_set("Asia/Jakarta");

        $tanggal_transaksi  = date("d F Y");

        // Include Gambar
        $nama_gambar    = "id-user-".$_SESSION['id_user']."-id-".rand(00,99).$_GET['id_produk']."-".$_FILES['bukti_transaksi']['name']; // mendapatkan nama bukti_transaksi
        $lokasi_gambar  = $_FILES['bukti_transaksi']['tmp_name']; // mendapatkan lokasi gambar
        $tujuan_gambar  = 'img/transaksi'; // pindah gambar tersebut ke lokasi ini
        $upload_gambar  = move_uploaded_file($lokasi_gambar, $tujuan_gambar.'/'.$nama_gambar); // function mengupload/memindahkan file ke direktori yang di maksud

        try {
            // fungsi untuk menambah data ke dalam databse
            $stmt = $pdo->prepare("INSERT INTO transaksi
                    (id_user,id_produk,qty,total_bayar,bukti_transaksi,tanggal_transaksi)
                    VALUES(:id_user,:id_produk,:qty,:total_bayar,:bukti_transaksi,:tanggal_transaksi)" );
                        
            $stmt->bindParam(":id_user", $id_user, PDO::PARAM_STR);
            $stmt->bindParam(":id_produk", $id_produk, PDO::PARAM_STR);
            $stmt->bindParam(":qty", $qty, PDO::PARAM_STR);
            $stmt->bindParam(":total_bayar", $total_bayar, PDO::PARAM_STR);
            $stmt->bindParam(":bukti_transaksi", $nama_gambar, PDO::PARAM_STR);
            $stmt->bindParam(":tanggal_transaksi", $tanggal_transaksi, PDO::PARAM_STR);

            $count = $stmt->execute();

            $insertId = $pdo->lastInsertId();

            echo "<script>alert('Berhasil!'); window.location = 'account.php?id_produk=$id_produk&id_transaksi=$insertId'</script>";
        }catch(PDOException $e){
            var_dump($e);
        }
    }

?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">
    <style type="text/css">
	body {
	  background-image: linear-gradient(to right, #4682B4, #00FFFF, #00FA9A);
	}
    .img {
        height: 200px;
        width: 50%;

    }
    </style>
</head>
<body>
<?php
include('nav.php');
?>
    <br><br><br><br>
    <div class="container bg-muted" id="bawah">
        <div class="wrapper">

    <?php if (isset($_POST['hitung'])): ?>

    <?php
        $total_bayar    = $_POST['qty']*$rProduk['harga'];
    ?>

    <div class="form">
        <h1 style="text-align: center;color: #23dbdb;">DETAIL TRANSAKSI PRODUK</h1>
        <br />
        <h4 style="text-align: center;">PRODUK YANG DI BELI</h4>
        <div class="foto">
            <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>">
            <h1><?= $rProduk['nama_produk']; ?></h1>
            <p>Rp<?= rp($rProduk['harga']); ?></p><br>
        </div>
        <br />
        <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
            <input type="hidden" class="form-control" id="id_produk" name="id_produk" value="<?= $rProduk['id_produk']; ?>">
            <div class="mb-3">
                <label for="qty" class="form-label">Qty</label>
                <input type="number" class="form-control" id="qty" name="qty" value="<?= $_POST['qty']; ?>" readonly>
            </div>
            <div class="mb-3">
                <label for="total_bayar" class="form-label">Total Bayar</label>
                <input type="text" class="form-control" id="total_bayar" name="total_bayar" value="Rp<?= rp($total_bayar); ?>" readonly>
            </div>
            <div class="mb-3">
                <label for="bukti_transaksi" class="form-label">Bukti Transaksi</label>
                <input class="form-control" type="file" id="bukti_transaksi" name="bukti_transaksi" required>
            </div>
            <br />
            <button type="submit" class="btn btn-primary" name="submit">CHECKOUT <i class="fas fa-shopping-cart"></i></button>
        </form>
    </div>

    <?php else: ?>

    <div class="form">
        <h1 style="text-align: center;color: #0000;">DETAIL TRANSAKSI PRODUK</h1>
        <br />
        <h4 style="text-align: center;">PRODUK YANG DI BELI</h4>
        <div class="foto">
            <img src="img/<?= $rProduk['gambar']; ?>" alt="<?= $rProduk['nama_produk']; ?>">
            <h1><?= $rProduk['nama_produk']; ?></h1>
            <p>Rp<?= rp($rProduk['harga']); ?></p><br>
        </div>
        <br />
        <form method="POST" action="" style="text-align: center;">
            <input type="hidden" class="form-control" id="id_produk" name="id_produk" value="<?= $rProduk['id_produk']; ?>">
            <div class="mb-3">
                <label for="qty" class="form-label">Qty</label>
                <input type="number" class="form-control" id="qty" name="qty" placeholder="Masukkan Qty" min="1" value="1" required>
                
                <button type="submit" class="btn btn-primary" name="hitung" style="margin-left: 0px;"> Lanjut<i class="fas fa-sync-alt"></i></button>
            </div>
        </form>
    </div>

    <?php endif ?>

</div>

       <!-- Option 1: Bootstrap Bundle with Popper -->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

</body>
</html>    
    