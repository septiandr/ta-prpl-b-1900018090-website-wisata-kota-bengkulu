<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="font/css/font-awesome.min.css">

</head>
<body>
	<!--Navigasi-->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top " id="mainNav">
        <div class="container">
          <a class="navbar-brand" href="#">Wisata<b>BENGKULU</b></a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="beli.html">Beli</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Login</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <!--nav-->
	<br><br><br><br>
	<div class="container" style="text-align: center;">
	<?php
$name = $no = $menu = $harga = $qty = $Pembayaran = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $nama = test_input($_POST["nama"]);
  $no = test_input($_POST["no"]);
  $alamat = test_input($_POST["alamat"]);
  $menu = test_input($_POST["Menu"]);
  $Pembayaran = test_input($_POST["Pembayaran"]);
}
if(isset($_POST['hitung'])){
  $nama = $_POST['nama'];
  $harga = $_POST['harga'];
  $qty = $_POST['qty'];
  $total = $harga*$qty;
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<div class="gs"  style="text-align: center-left;">
	<?php
echo "<h2>VALIDASI DATA:</h2>";
echo "<b>Nama</b>: ",$nama;
echo "<br>";
echo "<b>No</b>: ",$no;
echo "<br>";
echo "<b>Alamat</b>: ",$alamat;
echo "<br>";
echo "<b>Menu</b> : ",$menu;
echo "<br>";
echo "<b>Harga</b> : Rp.",$harga;
echo "<br>";
echo "<b>Jumlah</b> : ",$qty;
echo "<br>";
echo "<b>Jenis Pembayaran</b>: ",$Pembayaran;
if($Pembayaran == "BNI (Otomatis)"){
	echo "<br> <b>Kode Pembayaran</b> : 1902983";
}
if($Pembayaran == "BRI (Otomatis)"){
	echo "<br> <b>Kode Pembayaran</b> : 7675555";
}
if($Pembayaran == "MANDIRI (Otomatis)"){
	echo "<br> <b>Kode Pembayaran</b> : 190081829";
}
if($Pembayaran == "DANA"){
	echo "<br> <b>Kode Pembayaran</b> : 1900018090";
}
echo "<br>";
echo "<b>total</b> : Rp.",$total;
echo "<br>";
?>
</div>
<div class="home">
	<?php
	$fp = fopen("data.txt", "a+");

	$nama = $_POST['nama'];
	$no = $_POST['no'];
	$alamat = $_POST['alamat'];
	$menu = $_POST['Menu'];
  $harga = $_POST['harga'];
  $qty = $_POST['qty'];
	$Pembayaran = $_POST['Pembayaran'];
 
	
	


	fputs($fp,"$nama|$no|$menu|$harga|$qty|$total|$Pembayaran\n");
	fclose($fp);

	
	?>
  <h2 class="text-danger">Silahkan Melakukan Pembayaran di Atas.</p>
	<a href="#" class="btn btn-primary">Bukti Transaksi</a>
	<a href="beli.html" class="btn btn-primary">Menu Pembelian</a>
</div>

	
</div>
</body>
</html>