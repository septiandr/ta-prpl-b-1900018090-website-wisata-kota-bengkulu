<?php

    require 'config.php';

    if (isset($_POST['submit'])) { // Jika sudah melakukan submit pendaftaran, maka program ini akan di jalankan

        // Di bawah ini untuk menampung inputan form yang diisi
        $nama_user  = $_POST['nama_user'];
        $email      = $_POST['email'];
        $password   = $_POST['password'];

        // Fungsi untuk mengecek apakah emailnya sudah pernah di daftarkan atau belum
        $queryCekEmail     = $pdo->query("SELECT email FROM user WHERE email='$email'");
        $rowsCekEmail      = $queryCekEmail->rowCount();
        $resultCekEmail    = $queryCekEmail->fetch(PDO::FETCH_ASSOC);

        // Jika emailnya belum pernah di daftarkan, maka boleh melakukan registrasi & program di bawah ini akan di jalankan
        if ($rowsCekEmail == 0){
            try {

                // fungsi untuk menambah data ke dalam databse
                $stmt = $pdo->prepare("INSERT INTO user
                        (nama_user,email,password)
                        VALUES(:nama_user,:email,:password)" );
                            
                $stmt->bindParam(":nama_user", $nama_user, PDO::PARAM_STR);
                $stmt->bindParam(":email", $email, PDO::PARAM_STR);
                $stmt->bindParam(":password", $password, PDO::PARAM_STR);

                $count = $stmt->execute();

                $insertId = $pdo->lastInsertId();

               echo "<script>alert('Berhasil daftar, silahkan login!'); window.location = 'login.php'</script>"; // jika berhasil mengirim email, maka akan di arahkan ke halaman ini
                
            }catch(PDOException $e){
                echo "<script>window.alert('Gagal daftar'); window.location(history.back(-1))</script>";
                die();
            }
        }else{
            echo "<script>window.alert('Email sudah ada!'); window.location(history.back())</script>";
            die();
        }
    }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Daftar Akun | WebBENGKULU</title>
    <link rel="stylesheet" href="admin/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
</head>

<body> 
    <nav>
        <label class="logo">WebBENGKULU <small style="font-size: 12px;">PortalUser</small></label> 
        <ul>
            <li><a href="index.php"><i class="fas fa-home"></i> Halaman Depan</a></li>
        </ul>
    </nav>
    <section>
        <div class="wrapper">
            <div class="form">
                <h3 style="text-align: center;">DAFTAR AKUN</h3>
                <br />
                <form method="POST" action="" enctype="multipart/form-data" style="text-align: center;">
                    <div class="mb-3">
                        <label for="nama_user" class="form-label">Nama User</label>
                        <input type="text" class="form-control" id="nama_user" name="nama_user" placeholder="Masukkan Nama User" required>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Alamat Email</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Alamat Email" required>
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" min="0" required>
                    </div>
                    <br />
                    <button type="submit" class="btn btn-primary" name="submit">Daftar <i class="fas fa-user-edit"></i></button>

                    <br />
                    <br />
                    <p>SUDAH PUNYA AKUN?</p>
                    <br />

                    <a href="login.php" role="button" class="btn btn-outline-primary">Yuk Login Aja <i class="fas fa-sign-in-alt"></i></a>
                </form>
            </div>
        </div>
    </section>
</body>
</html>